
const mysql = require('mysql');

const connectionName = 'calm-streamer-190422:us-east1:sql-dest';
const dbUser = 'root';
const dbPass = 'CMTOu2018';
const dbName = 'inventory';

const pool = mysql.createPool({
    connectionLimit : 1,
    socketPath: '/cloudsql/' + connectionName,
    user: dbUser,
    password: dbPass,
    database: dbName
});

/**
    getInventory: dump the contents of the inventory table as JSON for testing
*/
exports.getInventory = (req, res) => {
    pool.query('SELECT id, value FROM product_inventory', function (error, results, fields) {
	if (error) throw error;
	res.send(JSON.stringify({ "inv": results }));
    });	    
};


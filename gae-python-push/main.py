
"""
Pull updates from Cloud SQL instance
"""

# [START gae_python_mysql_app]
import os
import json
import logging
import MySQLdb
import jinja2
import webapp2
from pubsub_utils import publish_to_topic

# the topic name will be included in the path, e.g. /events/inventory_updates
EVENTS_PREFIX = '/events/'

# These environment variables are configured in app.yaml.
CLOUDSQL_CONNECTION_NAME = os.environ.get('CLOUDSQL_CONNECTION_NAME')
CLOUDSQL_USER = os.environ.get('CLOUDSQL_USER')
CLOUDSQL_PASSWORD = os.environ.get('CLOUDSQL_PASSWORD')
CLOUDSQL_DB = os.environ.get('CLOUDSQL_DB')

# configure Jinja for templating
JINJA_ENVIRONMENT = jinja2.Environment(
	loader=jinja2.FileSystemLoader(os.path.dirname(__file__)),
	extensions=['jinja2.ext.autoescape'],
	autoescape=True)

# decide what database to connect to based on if we are running locally or a deployed GAE app
def connect_to_cloudsql():

	if os.getenv('SERVER_SOFTWARE', '').startswith('Google App Engine/'):	
		# Connect using the unix socket
		cloudsql_unix_socket = os.path.join('/cloudsql', CLOUDSQL_CONNECTION_NAME)

		db = MySQLdb.connect(
			unix_socket=cloudsql_unix_socket, user=CLOUDSQL_USER, passwd=CLOUDSQL_PASSWORD,
			db=CLOUDSQL_DB)

	# If the unix socket is unavailable, this means that we are running locally and 
	# need to proxy our connection with cloud_sql_proxy:
	#
	#   $ cloud_sql_proxy -instances=calm-streamer-190422:us-west1:sql-source=tcp:3306

	else:
		db = MySQLdb.connect(host='127.0.0.1', user=CLOUDSQL_USER, passwd=CLOUDSQL_PASSWORD,
				db=CLOUDSQL_DB)

	return db

# fetch unprocessed records, call the passed lambda with each row
# returns total records and max(updated_on) 
def fetch_records(f):
	db = connect_to_cloudsql()
	cursor = db.cursor(MySQLdb.cursors.DictCursor)
	cursor.execute(
		'select * from product_inventory_log where is_processed=0 order by updated_on')
	records = 0
	last_date = None
	for r in cursor.fetchall():
		f(r)
		records = records + 1
		last_date = r['updated_on']
	cursor.close()
	return (records, last_date)

# update is_processed for the records we've just published in the queue
#  otherwise they'll get picked up again the next time the cron job fires this handler
def mark_as_processed(last_date):
	db = connect_to_cloudsql()
	db.autocommit(False)
	cursor = db.cursor()
	try:
		num_rows = cursor.execute(
			"update product_inventory_log set is_processed=1 where " \
			"(is_processed=0 and updated_on <= %s)", (str(last_date),))
		db.commit()
		logging.info('Updated {} row(s)'.format(num_rows));
	except:
		db.rollback()
	cursor.close()


# upsert a single (id, value) into the inventory database for testing
def upsert_single(id, value):
	db = connect_to_cloudsql()
	db.autocommit(False)
	cursor = db.cursor()
	try:
		cursor.execute('replace into product_inventory (id, value) ' \
				'values (%s, %s)', (id, value))
		db.commit()
		logging.info('Upsert for {}={}'.format(id, value))
	except:
		db.rollback()
	cursor.close()

# default handler for /, just dump out the data for debugging
# this doesn't update the database so it's safe to hit repeatedly
class MainPage(webapp2.RequestHandler):
	def get(self):
		self.response.headers['Content-Type'] = 'text/plain'
		self.response.write('Records not yet processed:\n');
		(num_recs, last_date) = fetch_records(
			lambda r: self.response.write('{}\n'.format(r)))
		self.response.write('\nTotal: {}, Last Date: {}'.format(num_recs, last_date))

# handle a test page that we can insert records into the database
class UpdatePage(webapp2.RequestHandler):
	def get(self):
		template_values = {
			'old_value': self.request.get('value', ''),
			'old_id': self.request.get('id', ''),
			'error': self.request.get('error', 0)
		}
        	template = JINJA_ENVIRONMENT.get_template('templates/form.html')
        	self.response.write(template.render(template_values))
	def post(self):
		id = self.request.get('id', '')
		try: 
			value = int(self.request.get('value', '')) 
		except ValueError:
			value = -1
		if len(id) > 0 and value >= 0:
			upsert_single(id, value)
		else:
			return self.redirect('/test?id={}&value={}&error=1'.format(id, value));  
		return self.redirect('/');

# event handler for cron-triggered requests, /event/*
# publish each row as a message on the provided topic name
# if there were records, mark them as processed
class CronEventHandler(webapp2.RequestHandler):
	def get(self):
		topic_name = self.request.path.split(EVENTS_PREFIX)[-1]
        	(num_recs, last_date) = fetch_records(
			lambda r: publish_to_topic(topic_name, msg=json.dumps({
				'id': r['id'], 'value': r['new_value'] })))
		logging.info('Processed {} messages'.format(num_recs));
		if num_recs > 0:
			mark_as_processed(last_date)
		# no content
		self.response.status = 204

# define routing for the application
app = webapp2.WSGIApplication([
	('/', MainPage),
	('/test', UpdatePage),
	('/events/.*', CronEventHandler),
], debug=True)

# [END gae_python_mysql_app]

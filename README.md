# MTP: Performance and Resiliency
Gavin Estey - CMTOu Class of 2018

![Architecture](https://bitbucket.org/sr_gavinestey/cmtou-mtp/raw/7e98f8c3e465998bc4612ee02bcf52568998e52e/Assets/Project_Architecture.png)

## Demo

[![asciicast](https://asciinema.org/a/fALCgqggwO930ZG9j3en8eqHq.png)](https://asciinema.org/a/fALCgqggwO930ZG9j3en8eqHq?speed=4)

## Contents

1. _gae-python-push_: A Google App Engine (GAE) application that runs a cron job every minute to pick up 
updates from a MySQL database (see _cloudsql_) and publish as Google Pub/Sub messages
2. _gcf-nodejs-handleMessage_: A Cloud Function (using NodeJS) that receives these messages and updates 
a secondary MySQL database
3. _cloudsql_: Example SQL for the MySQL Cloud SQL instances

### Assets

In the Assets/ folder you can find useful assets:

* _Whitepaper.pdf_: Whitepaper
* _Whitepaper_Images_: Images from whitepaper (diagrams exported as PNGs)
* _Diagrams.sdxml_: [Simple Diagram](https://www.simplediagrams.com/)
* _Cloud Integration.pptx_: Powerpoint deck with architecture examples to reuse 

Read the [Whitepaper](https://docs.google.com/document/d/1tptBbAjDUUn9Lm_pwMdWtB0MF92HRSOys--eXlj5ce4/edit?usp=sharing "Link to Whitepaper on Google Docs")

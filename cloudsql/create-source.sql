
# SQL to create tables for source DB

create table product_inventory_log (id varchar(255) not null , new_value int not null, 
	old_value int not null , updated_on datetime , is_processed tinyint(1) not null default 0);

create index index_product_inventory_log_is_processed on product_inventory_log (is_processed);

# Triggers

delimiter $$

create trigger inventory_insert after insert on product_inventory
for each row 
begin
 insert into product_inventory_log values (new.id, new.value, 0, now(), 0);
end 
$$

create trigger inventory_update after update on product_inventory
for each row 
begin
 if (new.value != old.value) 
 then 
 	insert into product_inventory_log values (new.id, new.value, old.value, now(), 0);
 end if;
end 
$$

delimiter ;
 

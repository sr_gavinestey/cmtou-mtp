
# Create Database

create database inventory;
use inventory;

# SQL to create tables

create table product_inventory (id varchar(255) not null primary key, value int not null default 0);


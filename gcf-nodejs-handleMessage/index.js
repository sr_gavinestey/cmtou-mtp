
const mysql = require('mysql');

const connectionName = 'calm-streamer-190422:us-east1:sql-dest';
const dbUser = 'root';
const dbPass = 'CMTOu2018';
const dbName = 'inventory';

const pool = mysql.createPool({
    connectionLimit : 1,
    socketPath: '/cloudsql/' + connectionName,
    user: dbUser,
    password: dbPass,
    database: dbName
});


/**
 * Triggered from a message on a Cloud Pub/Sub topic.
 *
 * @param {!Object} event Event payload and metadata.
 * @param {!Function} callback Callback function to signal completion.
 */
exports.handleMessage = (event, callback) => {
  const pubsubMessage = event.data;
  const payload = Buffer.from(pubsubMessage.data, 'base64').toString();
  const message = JSON.parse(payload);
  if (message['id'] != undefined && message['value'] != undefined) {
   	pool.query('replace into product_inventory(id, value) values (?,?)', 
        [message.id, message.value], 
        function (error, results, fields) {
    		if (error) throw error;
      		console.log('Updated "' + message.id + '" to ' + message.value);
    	});
  } else {
   	console.log('Bad payload: ' + payload);
  }
  callback();
};

